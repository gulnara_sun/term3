#include <vector>
#include <iostream>
#include <set>
#include <stdint.h>
#include <string>



using std::cin;
using std::cout;
using std::vector;
using std::set;
using std::string;


/////////////////////////////B1/////////////////////////////

char next_letter(set<char>& forbidden_letters) {
    for (char c = 'a'; c <= 'z'; ++c) {
        if (forbidden_letters.find(c) == forbidden_letters.end()) {
            return c;
        }
    }
}

void string_from_p(vector<uint32_t>& p, string& s) {
    set<char> forbidden_letters;
    s.resize(p.size());
    s[0] = 'a';
    for (uint32_t i = 1; i < p.size(); ++i) {
        forbidden_letters.clear();
        if (p[i] == 0) {
            uint32_t k = p[i - 1];
            forbidden_letters.insert(s[k]);
            while(k > 0) {
                k = p[k - 1];
                forbidden_letters.insert(s[k]);
            }
            s[i] = next_letter(forbidden_letters);
        } else {
            s[i] = s[p[i] - 1];
        }
    }
}

/////////////////////////////B2/////////////////////////////

vector<uint32_t> z_to_p(vector<uint32_t>& z);

void string_from_z(vector<uint32_t>& z, string& s) {
    vector<uint32_t> p;
    p = z_to_p(z);
    string_from_p(p, s);
}

/////////////////////////////B3/////////////////////////////

vector<uint32_t> calc_p(string str) {
    vector<uint32_t> p;
    p.resize(str.size());
    p[0] = 0;
    for (uint32_t i = 1; i < str.size(); ++i) {
        uint32_t k = p[i - 1];
        while(k > 0 && str[i] != str[k]) {
            k = p[k - 1];
        }
        if (str[i] == str[k]) {
            p[i] = k + 1;
        } else {
            p[i] = 0;
        }
    }
    return p;
}

/////////////////////////////B4/////////////////////////////


vector<uint32_t> calc_z(string str) {
    vector<uint32_t> z;
    z.assign(str.size(), 0);
    uint32_t r = 0;
    uint32_t l = 0;
    z[0] = str.size();
    for (uint32_t i = 1; i < str.size(); ++i) {
        if (i < r) {
            z[i] = std::min(z[i - l], r - i);
        }
        while (str[i + z[i]] == str[z[i]] && i + z[i] < str.size()) {
            ++z[i];
        }
        if (i + z[i] > r) {
            r = i + z[i];
            l = i;
        }

    }
    return z;
}

/////////////////////////////B5/////////////////////////////

vector<uint32_t> p_to_z(vector<uint32_t>& p) {
    string s;
    string_from_p(p ,s);
    return calc_z(s);
}

/////////////////////////////B6/////////////////////////////

vector<uint32_t> z_to_p(vector<uint32_t>& z) {
    vector<uint32_t> p;
    z[0] = 0;
    p.assign(z.size(), 0);
    for (uint32_t i = 0; i < z.size(); ++i) {
        if (z[i] != 0) {
            for (int j = z[i] - 1; j >= 0 && (p[i + j] == 0); --j) {
                p[i + j] = j + 1; // j + 1
            }
        }
    }
    return p;
}

////////////////////////////MAIN////////////////////////////


int main() {
    string s;
    vector<uint32_t> z;
    int j;
    while(cin >> j) {
        z.push_back(j);
    }
    string_from_z(z, s);
    cout << s;
    return 0;
}

