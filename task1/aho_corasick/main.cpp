#include <iostream>
#include <vector>
#include <string>
#include <stdint.h>
#include <queue>
#include <utility>

using std::vector;
using std::pair;
using std::string;
using std::queue;

const uint32_t ALPHABET_SIZE = 26;

//////////////////////////////////node//////////////////////////////////

struct node {
    node(uint32_t parent, char letter) :
            parent(parent),
            is_terminate(false),
            suff(UINT32_MAX),
            short_suff(UINT32_MAX),
            last_letter(letter),
            node_patterns() {
        for (uint32_t i = 0; i < ALPHABET_SIZE; ++i) {
            next_by_edge[i] = UINT32_MAX;
            move_by[i] = UINT32_MAX;
        }
    }
    uint32_t parent;
    bool is_terminate = false;
    uint32_t suff;
    uint32_t short_suff;
    uint32_t next_by_edge[ALPHABET_SIZE];
    uint32_t move_by[ALPHABET_SIZE];
    char last_letter;
    vector<uint32_t> node_patterns;
};

//////////////////////////////////KeywordTree//////////////////////////////////

class KeywordTree {
public:
    KeywordTree() {
        node* root = new node(UINT32_MAX, 0);
        nodes.push_back(root);
    }
    uint32_t get_move(uint32_t node_ind, uint32_t letter);
    uint32_t get_short_suff(uint32_t node_ind);

    vector<node*> nodes;
    vector<uint32_t> words;

private:
    uint32_t get_suff(uint32_t node_ind);

};

//////////////////////////////////AhoCorasick//////////////////////////////////

class AhoCorasick {
public:
    AhoCorasick() : size_of_mask(0) {}
    void find_mathces(vector<uint32_t>& answer, string& text);
    void add_mask_char(char const &ch, uint32_t ind_in_mask, uint32_t& cur_node, string& cur_str);
private:
    KeywordTree trie;
    uint32_t size_of_mask;
};



//////////////////////////////////main//////////////////////////////////

int main() {
    uint32_t c;
    AhoCorasick ak;
    uint32_t cur_node = 0;
    uint32_t ind_in_mask = 0;
    string cur_submask;
    while((c = getchar()) && ((c >= 'a' && c <= 'z') || c == '?')) {
        ak.add_mask_char(c, ind_in_mask, cur_node, cur_submask);
        ++ind_in_mask;
    }
    ak.add_mask_char(c, ind_in_mask, cur_node, cur_submask);
    string text;
    std::cin >> text;
    vector<uint32_t> answers;
    ak.find_mathces(answers, text);
    for(auto& i: answers) {
        std::cout << i << " ";
    }
    return 0;
}

//////////////////////////////////AhoCorasick//////////////////////////////////

void AhoCorasick::find_mathces(vector<uint32_t>& answer, string& text) {
    answer.clear();
    uint32_t size_of_text = text.size();
    if (trie.words.empty()) {
        if (size_of_text >= size_of_mask) {
            for (uint32_t i = 0; i < size_of_text - size_of_mask + 1; ++i) {
                answer.push_back(i);
            }
        }
        return;
    }
    vector<uint32_t> counter(size_of_text);
    uint32_t cur_node = 0;
    uint32_t u;
    for(uint32_t i = 0; i < size_of_text; ++i) {
        cur_node = trie.get_move(cur_node, text[i] - 'a');
        u = cur_node;
        uint32_t pos_in_mask;
        uint32_t word_size;
        while (u != 0) {
            for (auto& pattern_ind: trie.nodes[u]->node_patterns) {
                pos_in_mask = trie.words[pattern_ind];
                if (i >= pos_in_mask && size_of_text - i >= size_of_mask - pos_in_mask) {
                    ++counter[i - pos_in_mask];
                }
            }
            u = trie.get_short_suff(u);
        }
    }
    for (uint32_t i = 0; i < size_of_text; ++ i) {
        if (counter[i] == trie.words.size()) {
            answer.push_back(i);
        }
    }
}

//////////////////////////////////KeywordTree//////////////////////////////////

uint32_t KeywordTree::get_suff(uint32_t cur_node) {
    if(nodes[cur_node]->suff == UINT32_MAX) {
        if (cur_node == 0 || nodes[cur_node]->parent == 0) {
            nodes[cur_node]->suff = 0;
        }
        else {
            nodes[cur_node]->suff = get_move(get_suff(nodes[cur_node]->parent), nodes[cur_node]->last_letter - 'a');
        }
    }
    return nodes[cur_node]->suff;
}

uint32_t KeywordTree::get_move(uint32_t cur_node, uint32_t letter) {
    if (nodes[cur_node]->move_by[letter] == UINT32_MAX) {
        if (nodes[cur_node]->next_by_edge[letter] != UINT32_MAX) {
            nodes[cur_node]->move_by[letter] = nodes[cur_node]->next_by_edge[letter];
        }
        else {
            if (cur_node == 0) {
                nodes[cur_node]->move_by[letter] = 0;
            }
            else {
                nodes[cur_node]->move_by[letter] = get_move(get_suff(cur_node), letter);
            }
        }
    }
    return nodes[cur_node]->move_by[letter];
}

uint32_t KeywordTree::get_short_suff(uint32_t cur_node) {
    if (nodes[cur_node]->short_suff == UINT32_MAX) {
        uint32_t node_suff = get_suff(cur_node);
        if (node_suff == 0) {
            nodes[cur_node]->short_suff = 0;
        }
        else if (nodes[node_suff]->is_terminate) {
            nodes[cur_node]->short_suff = node_suff;
        }
        else {
            nodes[cur_node]->short_suff = get_short_suff(get_suff(cur_node));
        }
    }
    return nodes[cur_node]->short_suff;
}

void AhoCorasick::add_mask_char(char const &ch, uint32_t ind_in_mask, uint32_t &cur_node, string &cur_str) {
    if ((ch >= 'a' && ch <= 'z') || ch == '?') {
        ++size_of_mask;
    }
    if (!(ch >= 'a' && ch <= 'z')) {
        if (!cur_str.empty()) {
            trie.words.push_back(ind_in_mask - 1);
            trie.nodes[cur_node]->is_terminate = true;
            trie.nodes[cur_node]->node_patterns.push_back((uint32_t)trie.words.size() - 1);
            cur_str = "";
        }
        cur_node = 0;
    }
    else {
        if (trie.nodes[cur_node]->next_by_edge[ch - 'a'] == UINT32_MAX) {
            node* new_node = new node(cur_node, ch);
            trie.nodes.push_back(new_node);
            trie.nodes[cur_node]->next_by_edge[ch - 'a'] = (uint32_t)trie.nodes.size() - 1;
        }
        cur_node = trie.nodes[cur_node]->next_by_edge[ch - 'a'];
        cur_str += ch;
    }
}
