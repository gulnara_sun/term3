#include <vector>
#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::vector;
using std::string;

vector<size_t> calc_z_func(const string& str) {
    vector<size_t> z(str.size(), 0);
    size_t r = 0;
    size_t l = 0;
    z[0] = str.size();
    for (size_t i = 1; i < str.size(); ++i) {
        if (i < r) {
            z[i] = std::min(z[i - l], r - i);
        }
        while (str[i + z[i]] == str[z[i]] && i + z[i] < str.size()) {
            ++z[i];
        }
        if (i + z[i] > r) {
            r = i + z[i];
            l = i;
        }
    }
    return z;
}

void find_pattern_occurrences(const string& pattern, const vector<size_t>& z_pattern, const string& s1,
        const string& s2, vector<size_t>& indices, size_t iteration) {
    string str = pattern + '#' + s1 + s2;
    size_t r = pattern.size();
    size_t l = pattern.size();
    size_t z_i = 0;
    for (size_t i = pattern.size() + 1; i < str.size() - s2.size(); ++i) {
        if (i < r) {
            z_i = std::min(z_pattern[i - l], r - i);
        }
        while (i + z_i < str.size() && str[i + z_i] == str[z_i]) {
            ++z_i;
        }
        if (i + z_i > r) {
            r = i + z_i;
            l = i;
        }
        if (z_i == pattern.size()) {
            indices.push_back(iteration * pattern.size() + i - pattern.size() - 1);
        }
    }
}

void read_string(string& str, size_t size, bool& eof) {
    str.clear();
    char letter;
    for (int j = 0; j < size; ++j) {
        if (cin >> letter) {
            str+= letter;
        } else {
            eof = true;
            break;
        }
    }
}

vector<size_t> process(string& pattern) {
    vector<size_t> indices;
    vector<size_t> z_pattern = calc_z_func(pattern);
    string s1, s2;
    bool eof = false;
    size_t iteration = 0;
    read_string(s1, pattern.size(), eof);
    if (!eof) {
        read_string(s2, pattern.size(), eof);
    }
    find_pattern_occurrences(pattern, z_pattern, s1, s2, indices, iteration);
    char letter;
    while (!eof) {
        ++iteration;
        std::swap(s1, s2);
        s2.clear();
        for (int j = 0; j < pattern.size(); ++j) {
            cin.get(letter);
            if ((letter != EOF) && (letter !='\n')) {
                s2 += letter;
            } else {
                eof = true;
                break;
            }
        }
        find_pattern_occurrences(pattern, z_pattern, s1, s2, indices, iteration);
    }
    return indices;
}

int main() {
    string pattern;
    cin >> pattern;
    vector<size_t> indices;
    indices = process(pattern);
    for (size_t i : indices) {
        cout << i << ' ';
    }
    return 0;
}