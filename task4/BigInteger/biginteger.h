#ifndef BIGINTEGER_H
#define BIGINTEGER_H

#include <iostream>
#include <vector>
#include <string>

using std::vector;
using std::string;
using std::max;
using std::min;

class BigInteger{
public:
    BigInteger() : is_positive(true) {}
    BigInteger(const int& value);
    BigInteger(const BigInteger& other);
    
    BigInteger &operator=(const BigInteger &other);

    BigInteger operator-() const;

    BigInteger &operator+=(const BigInteger &other);
    BigInteger &operator-=(const BigInteger &other);
    BigInteger &operator*=(const BigInteger &other);
    BigInteger &operator/=(const BigInteger &other);
    BigInteger &operator%=(const BigInteger &other);

    BigInteger &operator++();
    const BigInteger operator++(int);
    BigInteger &operator--();
    const BigInteger operator--(int);

    bool operator==(const BigInteger &other) const;
    bool operator!=(const BigInteger &other) const;
    bool operator>(const BigInteger &other) const;
    bool operator<(const BigInteger &other) const;
    bool operator<=(const BigInteger &other) const;
    bool operator>=(const BigInteger &other) const;

    friend BigInteger operator+(const BigInteger &first, const BigInteger &second);
    friend BigInteger operator-(const BigInteger &first, const BigInteger &second);
    friend BigInteger operator*(const BigInteger &first, const BigInteger &second);
    friend BigInteger operator/(const BigInteger &first, const BigInteger &second);
    friend BigInteger operator%(const BigInteger &first, const BigInteger &second);

    friend std::istream &operator>>(std::istream &is, BigInteger &value);
    friend std::ostream &operator<<(std::ostream &os, const BigInteger &value);

    explicit operator bool() const;

    string to_string() const;
private:
    BigInteger abs() const;
    int to_int() const;
    void delete_zeros();
    vector<int> nums;
    bool is_positive;
};

BigInteger::BigInteger(const int& value) {
    is_positive = value >= 0;
    if (value == 0) {
        nums = {0};
    }
    else {
        int tmp = value * (is_positive ? 1 : -1);
        while (tmp > 0) {
            nums.push_back(tmp % 10);
            tmp /= 10;
        }
    }
}

BigInteger::BigInteger(const BigInteger &other) : nums(other.nums), is_positive(other.is_positive){}

BigInteger BigInteger::abs() const {
    BigInteger answer(*this);
    answer.is_positive = true;
    return answer;
}

void BigInteger::delete_zeros() {
    while (nums.back() == 0 && nums.size() > 1) {
        nums.pop_back();
    }
}

BigInteger& BigInteger::operator=(const BigInteger &other) {
    if (this == &other) {
        return *this;
    }
    nums = other.nums;
    is_positive = other.is_positive;
    return *this;
}

bool BigInteger::operator==(const BigInteger &other) const {
    BigInteger other_copy(other);
    other_copy.delete_zeros();
    BigInteger this_copy(*this);
    this_copy.delete_zeros();
    return (is_positive == other.is_positive) && (this_copy.nums == other_copy.nums);
}

bool BigInteger::operator<(const BigInteger &other) const {
    if (!is_positive) {
        return other.is_positive ? true : other.abs() < (*this).abs();
    }
    if ((is_positive && !other.is_positive) || nums.size() > other.nums.size() || *this == other) {
        return false;
    }
    if (nums.size() < other.nums.size()) {
        return true;
    }
    bool is_less = true;
    for (int i = nums.size() - 1; i >=0; --i) {
        if (nums[i] > other.nums[i]) {
            is_less = false;
            break;
        }
        if (nums[i] < other.nums[i]) {
            is_less = true;
            break;
        }
    }
    return is_less;
}

bool BigInteger::operator>(const BigInteger &other) const {
    return other < *this;
}

bool BigInteger::operator!=(const BigInteger &other) const {
    return !(*this == other);
}

bool BigInteger::operator<=(const BigInteger &other) const {
    return !(*this > other);
}

bool BigInteger::operator>=(const BigInteger &other) const {
    return !(*this < other);
}

int BigInteger::to_int() const {
    int value = 0;
    int power = 1;
    for (size_t i = 0; i < nums.size(); ++i) {
        value += nums[i] * power;
        power *= 10;
    }
    if (!is_positive) {
        value *= -1;
    }
    return value;
}

BigInteger& BigInteger::operator+=(const BigInteger &other) {
    if (is_positive && !other.is_positive) {
        return *this -= other.abs();
    }
    if (!is_positive && other.is_positive) {
        BigInteger tmp(*this);
        *this = other;
        return *this -= tmp.abs();
    }
    int carry = 0;
    int new_carry = 0;
    size_t i = 0;
    for (; i < min(nums.size(), other.nums.size()); ++i) {
        new_carry = (nums[i] + other.nums[i] + carry >= 10) ? 1 : 0;
        nums[i] = (nums[i] + other.nums[i] + carry) % 10;
        carry = new_carry;
    }
    const BigInteger* larger = nums.size() > other.nums.size() ? this : &other;
    nums.resize(larger->nums.size());
    for (; i < nums.size(); ++i) {
        new_carry = (larger->nums[i] + carry >= 10) ? 1 : 0;
        nums[i] = (larger->nums[i] + carry) % 10;
        carry = new_carry;
    }
    if (carry) {
        nums.push_back(1);
    }
    return *this;
}

//BigInteger& BigInteger::operator-=(const BigInteger &other) {
//    if (is_positive != other.is_positive) {
//        *this += -other;
//    }
//    else if (*this >= other) {
//        long long current = 0;
//        for (int i = 0; current > 0 || i < static_cast<int>(other.nums.size()); ++i) {
//            nums[i] -= current + (i < static_cast<int>(other.nums.size()) ? other.nums[i] : 0);
//            if (nums[i] < 0){
//                current = 1;
//                nums[i] += 10;
//            }
//            else {
//                current = 0;
//            }
//        }
//        delete_zeros();
//    }
//    else {
//        BigInteger buffer = *this;
//        *this = other;
//        *this -= buffer;
//        is_positive *= -1;
//    }
//    return *this;
//}


BigInteger& BigInteger::operator-=(const BigInteger &other) {
    if (is_positive && !other.is_positive) {
        BigInteger tmp(other);
        tmp.is_positive = true;
        return *this += tmp;
    }
    if (!is_positive && other.is_positive) {
        is_positive = true;
        *this += other;
        is_positive = false;
        return *this;
    }
    if (!is_positive && !other.is_positive) {
        BigInteger tmp(*this);
        tmp.is_positive = true;
        *this = other;
        this->is_positive = true;
        return *this -= tmp;
    }
    if (*this < other) {
        BigInteger tmp(other);
        tmp -= *this;
        *this = tmp;
        this->is_positive = false;
        return *this;
    }
    int carry = 0;
    int new_carry = 0;
    size_t i = 0;
    for (; i < min(nums.size(), other.nums.size()); ++i) {
        new_carry = nums[i] + carry < other.nums[i] ? -1 : 0;
        nums[i] = nums[i] + carry - other.nums[i] + (new_carry == -1 ? 10 : 0);
        carry = new_carry;
    }
    for (; i < nums.size(); ++i) {
        new_carry = nums[i] + carry < 0 ? -1 : 0;
        nums[i] = nums[i] + carry + (new_carry == -1 ? 10 : 0);
        carry = new_carry;
    }
    delete_zeros();
    return *this;
}

BigInteger& BigInteger::operator*=(const BigInteger &other) {
    if (this->nums.size() > other.nums.size()) {
        BigInteger tmp(other);
        tmp *= *this;
        return *this = tmp;
    }
    size_t n = other.nums.size();
    if (n < 5) {
        BigInteger tmp;
        int v = this->to_int() * other.to_int();
        tmp = v;
        return *this = tmp;
    }
    BigInteger answer;

    if (other.is_positive != is_positive) {
        answer = this->abs() * other.abs();
        answer.is_positive = (answer == 0);
        return *this = answer;
    }
    answer.nums.assign(nums.size() + other.nums.size() + 1, 0);
    nums.resize(n);

    BigInteger a1(*this);
    BigInteger a2(*this);
    a2.nums.resize(n / 2);
    a1.nums.erase(a1.nums.begin(), a1.nums.begin() + n / 2);
    a1.delete_zeros();
    a2.delete_zeros();

    BigInteger b1(other);
    BigInteger b2(other);
    b2.nums.resize(n / 2);
    b1.nums.erase(b1.nums.begin(), b1.nums.begin() + n / 2);
    b1.delete_zeros();
    b2.delete_zeros();
    BigInteger tmp1 = a1 * b1;
    BigInteger tmp2 = a2 * b2;
    BigInteger tmp3 = (a1 + a2)*(b1 + b2) - tmp1 - tmp2;
    tmp1.nums.insert(tmp1.nums.begin(), 2 * (n / 2), 0);
    tmp3.nums.insert(tmp3.nums.begin(), n / 2, 0);
    answer = tmp1 + tmp3 + tmp2;
    answer.delete_zeros();
    return *this = answer;
}

BigInteger& BigInteger::operator/=(const BigInteger &other) {
    if (other == 0) {
        return *this;
    }
    if (this->abs() < other.abs()) {
        return *this = 0;
    }
    size_t n = max(nums.size(), other.nums.size());
    if (n < 9) {
        BigInteger answer;
        answer = this->to_int() / other.to_int();
        return *this = answer;
    }
    BigInteger answer;
    answer.is_positive = (other.is_positive == is_positive);
    answer.nums.assign(nums.size(), 0);
    BigInteger cur;
    for (int i = int(nums.size()) - 1; i >= 0; --i) {
        cur.nums.insert(cur.nums.begin(), nums[i]);
        cur.delete_zeros();
        int res_num = 0;
        int left = 0;
        int right = 10 - 1;
        while (left <= right) {
            const int mid = (left + right) / 2;
            BigInteger cur_product(0);
            cur_product = other.abs() * mid;
            if (cur_product <= cur) {
                res_num = mid;
                left = mid + 1;
            } else
                right = mid - 1;
        }
        answer.nums[i] = res_num;
        cur -= other.abs() * res_num;
    }
    answer.delete_zeros();
    return *this = answer;
}

BigInteger& BigInteger::operator%=(const BigInteger &other) {
    return *this = (*this - ((*this / other) * other));
}

BigInteger operator+(const BigInteger &first, const BigInteger &second) {
    BigInteger answer(first);
    answer += second;
    return answer;
}

BigInteger operator-(const BigInteger &first, const BigInteger &second) {
    BigInteger answer(first);
    answer -= second;
    return answer;
}

BigInteger operator*(const BigInteger &first, const BigInteger &second) {
    BigInteger answer(first);
    answer *= second;
    return answer;
}

BigInteger operator/(const BigInteger &first, const BigInteger &second) {
    BigInteger answer = first;
    answer /= second;
    return answer;
}

BigInteger operator%(const BigInteger &first, const BigInteger &second) {
    BigInteger answer = first;
    answer %= second;
    return answer;
}

BigInteger BigInteger::operator-() const {
    BigInteger answer(*this);
    if (nums.size() == 1 && nums[0] == 0) {
        answer.is_positive = true;
        return answer;
    }
    else {
        answer.is_positive = !answer.is_positive;
        return answer;
    }
}

BigInteger &BigInteger::operator++() {
    return (*this += 1);
}

const BigInteger BigInteger::operator++(int) {
    BigInteger answer = *this;
    ++(*this);
    return answer;
}

BigInteger &BigInteger::operator--() {
    return (*this -= 1);
}

const BigInteger BigInteger::operator--(int) {
    BigInteger answer = *this;
    --(*this);
    return answer;
}

BigInteger::operator bool() const {
    return *this != 0;
}

string BigInteger::to_string() const {
    string str;
    if (!is_positive) {
        str += "-";
    }
    for (int i = nums.size() - 1; i >= 0; --i) {
        str += static_cast<char>('0' + nums[i]);
    }
    
    return str;
}

std::istream &operator>>(std::istream &is, BigInteger &value) {
    string str;
    value.nums.clear();
    is >> str;
    if (str == "0" || str == "-0" || str.empty()) {
        value.nums = {0};
        value.is_positive = true;
        return is;
    }
    int is_negative = 0;
    if (str[0] == '-') {
        value.is_positive = false;
        ++is_negative;
    } else {
        value.is_positive = true;
    }
    value.nums.reserve(str.length() - is_negative);
    for (int i = str.length() - 1; i >= is_negative; --i) {
        value.nums.push_back(static_cast<int>(str[i]) - '0');
    }
    return is;
}

std::ostream &operator<<(std::ostream &os, const BigInteger &value) {
    os << value.to_string();
    return os;
}

#endif