#include <algorithm>
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <stack>
#include <unordered_set>
#include <set>

#define PI 3.14159265

using std::vector;
using std::pair;
using std::cin;
using std::cout;

/***************************************************POINT***************************************************/

struct Point {
    Point(){};
    Point(double x, double y) : x(x), y(y) {};
    double x;
    double y;
    double length() const;
};


std::istream& operator>>(std::istream& in, Point& vector) {
    double x, y;
    in >> x >> y;
    vector.x = x;
    vector.y = y;
    return in;
}

std::ostream& operator<<(std::ostream& out, const Point& vector) {
    out << vector.x << " " << vector.y << '\n';
    return out;
}

inline bool operator<(const Point& first, const Point& second) {
    bool rv = false;
    if (first.y < second.y) {
        rv = true;
    }
    else if (first.y == second.y && first.x < second.x) {
        rv = true;
    }
    return rv;
}

const Point operator+(const Point &first, const Point &second) {
    return {first.x + second.x, first.y + second.y};
}

const Point operator-(const Point &first, const Point &second) {
    return {first.x - second.x, first.y - second.y};
}

/***************************************************POINT***************************************************/


double calc_angle(const Point& first, const Point& second) {
    double arctg = atan2(second.y - first.y, second.x - first.x);
    return arctg < 0 ? arctg + 2*PI : arctg;
}

vector<Point> calc_minkowski_sum(const vector<Point>& first, const vector<Point>& second) {
    vector<Point> answer;
    size_t i = 0;
    size_t j = 0;
    while(i < first.size() - 1 || j < second.size() - 1) {
        answer.push_back(first[i] + second[j]);
        double angle1 = calc_angle(first[i], first[i + 1]);
        double angle2 = calc_angle(second[j], second[j + 1]);
        if (angle1 < angle2 && i < first.size() - 1) {
            ++i;
        }
        else if (angle1 > angle2 && j < second.size() - 1) {
            ++j;
        }
        else {
            if (i < first.size() - 1) ++i;
            if (j < second.size() - 1) ++j;
        }
    }
    return answer;
}

double count_triangle_area(const Point& p1, const Point& p2) {
    return std::abs(p1.x * p2.y - p1.y * p2.x) / 2;
}

bool check_point_inside(const Point& point, const vector<Point>& polygon) {
    double space1 = 0;
    double space2 = 0;

    for (auto i = 0; i < polygon.size() - 1; ++i) {
        space1 += count_triangle_area(polygon[i] - point, polygon[(i + 1) % (polygon.size() - 1)] - point);
    }
    for (auto i = 0; i < polygon.size() - 1; ++i) {
        space2 += count_triangle_area(polygon[i] - polygon[0], polygon[(i + 1) % (polygon.size() - 1)] - polygon[0]);
    }
    return std::abs(space1 - space2) < 1e-7;
}


int main() {
    size_t n, m;
    cin >> n;
    vector<Point> first(n);
    for (int j = 0; j < n; ++j) {
        cin >> first[j];
    }
    rotate(first.begin(),
           std::min_element(first.begin(), first.end()),
           first.end());
    first.push_back(first[0]);
    cin >> m;
    vector<Point> second(m);
    for (int j = 0; j < m; ++j) {
        cin >> second[j];
        second[j].x *= -1;
        second[j].y *= -1;
    }
    std::reverse(std::begin(first), std::end(first));
    std::reverse(std::begin(second), std::end(second));
    rotate(second.begin(),
           std::min_element(second.begin(), second.end()),
           second.end());
    second.push_back(second[0]);
    vector<Point> sum = calc_minkowski_sum(first, second);
    Point zero(0, 0);
    cout << (check_point_inside(zero, sum) ? "YES" : "NO");
    return 0;
}