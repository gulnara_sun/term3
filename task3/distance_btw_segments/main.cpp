#include <cmath>
#include <iostream>
#include <iomanip>

struct Vector {
    Vector(){};
    Vector(double x, double y, double z) : x(x), y(y), z(z) {};
    double x;
    double y;
    double z;
};

std::istream &operator>>(std::istream &in, Vector& vector) {
    double x, y, z;
    in >> x >> y >> z;
    vector.x = x;
    vector.y = y;
    vector.z = z;
    return in;
}

std::ostream &operator<<(std::ostream &out, const Vector& vector) {
    out << vector.x << " " << vector.y << " " << vector.z << '\n';
    return out;
}

const Vector operator+(const Vector &first, const Vector &second) {
    return {first.x + second.x, first.y + second.y, first.z + second.z};
}

const Vector operator-(const Vector &first, const Vector &second) {
    return {first.x - second.x, first.y - second.y, first.z - second.z};
}

struct Segment {
    Segment(){};
    Segment(Vector start, Vector end) : start(start), end(end) {};
    Vector start;
    Vector end;
    Vector get_point(double rate) const {
        return {start.x + (end.x - start.x) * rate,
                start.y + (end.y - start.y) * rate,
                start.z + (end.z - start.z) * rate};
    }
};

std::istream &operator>>(std::istream &is, Segment &segment) {
    is >> segment.start >> segment.end;
    return is;
}

std::ostream &operator<<(std::ostream &out, const Segment& segment) {
    out << segment.start << segment.end;
    return out;
}

template <typename F>
double tern_search(F function, double left, double right, double eps = 1e-10) {
    while (right - left > eps) {
        double mid1 = left + (right - left) / 3;
        double mid2 = right - (right - left) / 3;
        if (function(mid2) - function(mid1) > eps) {
            right = mid2;
        }
        else {
            left = mid1;
        }
    }
    return function(left);
}

class CalcSegmentsDistance {
public:
    CalcSegmentsDistance(Segment& first, Segment& second) : first(first), second(second), min_dist(INFINITY) {
        find_min_distance();
    }
    double get_min_dist() {
        return min_dist;
    }
private:
    double calc_dot(const Vector first, const Vector second);
    double calc_points_distance(const Vector& first, const Vector& second);
    double find_min_distance();
    double find_point_segment_min_dist(const Vector& point, const Segment& segment);
    Segment first;
    Segment second;
    double min_dist;
};

double CalcSegmentsDistance::calc_dot(const Vector first, const Vector second) {
    return first.x * second.x + first.y * second.y + first.z * second.z;
}

double CalcSegmentsDistance::calc_points_distance(const Vector &first, const Vector &second) {
    double rv =  sqrt(calc_dot(second - first, second - first));
    return rv;
}

double CalcSegmentsDistance::find_point_segment_min_dist(const Vector& point, const Segment& segment) {
    return tern_search([this, point, segment](double rate)
           {return calc_points_distance(point, segment.get_point(rate));},
           0.0, 1.0);
}

double CalcSegmentsDistance::find_min_distance() {
    min_dist = tern_search([this](double rate)
               {return find_point_segment_min_dist(first.get_point(rate), second);},
               0.0, 1.0);
}

int main() {
    std::cout << std::fixed << std::setprecision(8);
    Segment s1;
    Segment s2;
    std::cin >> s1 >> s2;
    CalcSegmentsDistance csd(s1, s2);
    std::cout << csd.get_min_dist();
}