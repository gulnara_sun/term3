#include <algorithm>
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <stack>
#include <unordered_set>
#include <set>

using std::vector;
using std::pair;
using std::get;

struct pairhash {
public:
    template <typename T, typename U>
    std::size_t operator()(const std::pair<T, U> &x) const
    {
        return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
    }
};

/***************************************************POINT***************************************************/

struct Point {
    Point(){};
    Point(double x, double y, double z) : x(x), y(y), z(z) {};
    double x;
    double y;
    double z;
    double length() const;
};

double Point::length() const {
    return sqrt(x*x +y*y + z*z);
}

std::istream& operator>>(std::istream& in, Point& vector) {
    double x, y, z;
    in >> x >> y >> z;
    vector.x = x;
    vector.y = y;
    vector.z = z;
    return in;
}

std::ostream& operator<<(std::ostream& out, const Point& vector) {
    out << vector.x << " " << vector.y << " " << vector.z << '\n';
    return out;
}

inline bool operator<(const Point& first, const Point& second) {
    bool rv = false;
    if (first.z < second.z) {
        rv = true;
    }
    else if (first.z == second.z) {
        if (first.x < second.x) {
            rv = true;
        }
        else if (first.x == second.x && first.y < second.y) {
            rv = true;
        }
    }
    return rv;
}

inline bool operator>(const Point& first, const Point& second) {
    return second < first;
}

const Point operator+(const Point &first, const Point &second) {
    return {first.x + second.x, first.y + second.y, first.z + second.z};
}

const Point operator*(const Point& point, const double &rate) {
    return {point.x * rate, point.y * rate, point.z * rate};
}

const Point operator-(const Point &first, const Point &second) {
    Point min_second = second * (-1);
    return first + min_second;
}

/***************************************************POINT***************************************************/

double calc_dot(const Point first, const Point second) {
    return first.x * second.x + first.y * second.y + first.z * second.z;
}

Point calc_cross(const Point first, const Point second) {
    return {first.y * second.z - first.z * second.y,
            first.z * second.x - first.x * second.z,
            first.x * second.y - first.y * second.x};
}


class GiftWrapping {
public:
    GiftWrapping(){};
    void make_hill(const vector<Point>& points_) {
        faces.clear();
        points = points_;
        find_first_face();
        wrap();
    }
    vector<vector<size_t>> get_hill();
private:
    void find_first_face();
    void wrap();
    void make_min_first(vector<size_t>& face);
    double calc_cos(const Point first, const Point second);
    size_t find_third_point(const size_t first, const size_t second, bool& right_order);
    size_t bend_over_edge(size_t e_first, size_t e_second, size_t third);
    vector<Point> points;
    std::set<vector<size_t>> faces;
};

/****************************************************MAIN*************************************************/

using std::cin;

int main() {
    size_t num_of_tests;
    size_t n;
    vector<Point> points;
    GiftWrapping gw;
    vector<vector<size_t>> faces;
    cin >> num_of_tests;
    for (int j = 0; j < num_of_tests; ++j) {
        cin >> n;
        points.resize(n);
        for (int i = 0; i < n; ++i) {
            cin >> points[i];
        }
        gw.make_hill(points);
        faces = gw.get_hill();
        std::cout << faces.size() << '\n';
        for (auto face : faces) {
            std::cout << 3 << ' ' << face[0] << ' ' << face[1] << " " << face[2] << '\n';
        }
    }
}

/****************************************************MAIN*************************************************/


/***********************************************GIFT_WRAPPING*********************************************/

double GiftWrapping::calc_cos(const Point first, const Point second) {
    return calc_dot(first, second)/first.length()/second.length();
}

size_t GiftWrapping::bend_over_edge(size_t e_first, size_t e_second, size_t third) {
    double maximal_dot_product = -1;
    double new_dot_product;
    size_t next_point = 0;
    Point normal = calc_cross(points[third] - points[e_second], points[e_first] - points[e_second]);
    normal = normal * (1 / normal.length());
    for (size_t i = 0; i < points.size(); i++) {
        if (i != e_first && i != e_second && i != third) {
            Point new_normal = calc_cross(points[e_second] - points[i], points[e_first] - points[e_second]);
            new_normal = new_normal * (1 / new_normal.length());
            new_dot_product = calc_dot(normal, new_normal);
            if (new_dot_product > maximal_dot_product) {
                next_point = i;
                maximal_dot_product = new_dot_product;
            }
        }
    }
    return next_point;
}

size_t GiftWrapping::find_third_point(const size_t first, const size_t second, bool& right_order) {
    for (size_t i = 0; i < points.size(); i++) {
        if (i != first && i != second) {
            bool i_is_ok = true;
            Point normal = calc_cross(points[second] - points[first], points[i] - points[first]);
            Point back_normal = normal * (-1);
            Point other_dot;
            for (size_t j = 0; j < points.size(); j++) {
                if (j != first && j != second && j != i) {
                    other_dot = points[j];
                    break;
                }
            }
            Point first_vector = points[first] + normal - other_dot;
            Point second_vector = points[first] - normal - other_dot;
            if (first_vector.length() < second_vector.length()) {
                normal = back_normal;
            }
            for (int j = 0; j < points.size(); ++j) {
                if (j != first && j != second && j != i) {
                    if ((points[first] + normal - points[j]).length() < (points[first] - normal - points[j]).length()) {
                        i_is_ok = false;
                        break;
                    }
                }
            }
            if (i_is_ok) {
                if (calc_dot(calc_cross(points[first] - points[second], points[i] - points[second]), normal) > 0) {
                    right_order = true;
                }
                else right_order = false;
                return i;
            }
        }
    }
}

void GiftWrapping::find_first_face() {
    double max_cos = -1;
    double cur_cos = max_cos;
    size_t first = std::distance(points.begin(), std::min_element(points.begin(), points.end()));
    size_t second = 0;
    for (size_t i = 0; i < points.size(); ++i) {
        if (i == first) {
            continue;
        }
        Point i_projection = {points[i].x, points[i].y, points[first].z};
        cur_cos = (points[i].x == points[first].x && points[i].y == points[first].y) ? 0 :
                calc_cos(points[i] - points[first], i_projection - points[first]);
        if (cur_cos > max_cos || (cur_cos == max_cos) && (points[i] > points[second])) {
            max_cos = cur_cos;
            second = i;
        }
    }
    bool right_order;
    size_t third = find_third_point(first, second, right_order);
    vector<size_t> face = {first, third, second};
    if (!right_order) {
        std::swap(face[1], face[2]);
    }
    make_min_first(face);
    faces.insert(face);
}


void GiftWrapping::make_min_first(vector<size_t>& face) {
    size_t min_point = *(std::min_element(face.begin(), face.end()));
    vector<size_t> tmp(3);
    while (face[0] != min_point) {
        tmp[1] = face[0];
        tmp[2] = face[1];
        tmp[0] = face[2];
        face = tmp;
    }
}

void GiftWrapping::wrap() {
    std::unordered_set<pair<size_t, size_t>, pairhash> processed;
    std::stack<std::tuple<size_t, size_t, size_t>> q;
    q.emplace(std::make_tuple((*faces.begin())[0], (*faces.begin())[1], (*faces.begin())[2]));
    q.emplace(std::make_tuple((*faces.begin())[1], (*faces.begin())[2], (*faces.begin())[0]));
    q.emplace(std::make_tuple((*faces.begin())[2], (*faces.begin())[0], (*faces.begin())[1]));
    while (!q.empty()) {
        auto cur = q.top();
        q.pop();
        if (processed.find(std::make_pair(get<0>(cur), get<1>(cur))) == processed.end() &&
                processed.find(std::make_pair(get<1>(cur), get<0>(cur))) == processed.end()) {
            size_t new_point = bend_over_edge(get<0>(cur), get<1>(cur), get<2>(cur));
            vector<size_t> face = {get<0>(cur), new_point, get<1>(cur)};
            make_min_first(face);
            faces.insert(face);
            q.emplace(std::make_tuple(get<0>(cur), new_point, get<1>(cur)));
            q.emplace(std::make_tuple(new_point, get<1>(cur), get<0>(cur)));
            processed.emplace(get<0>(cur), get<1>(cur));
        }
    }
}

bool cmp(const vector<size_t> &a, const vector<size_t> &b){
    bool less = false;
    if (a[0] < b[0]) {
        less = true;
    }
    else if (a[0] == b[0]) {
        if (a[1] < b[1]) {
            less = true;
        }
        else if (a[1] == b[1]) {
            if (a[2] < b[2]) {
                less = true;
            }
        }
    }
    return less;
}

vector<vector<size_t>> GiftWrapping::get_hill() {
    vector<vector<size_t>> tmp(faces.begin(), faces.end());
    std::sort(tmp.begin(), tmp.end(), cmp);
    return tmp;
}

/***********************************************GIFT_WRAPPING*********************************************/
