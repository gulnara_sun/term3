#include <algorithm>
#include <cmath>
#include <vector>
#include <iostream>
#include <string>

const size_t ALPHABET_SIZE = 27;

using std::vector;
using std::max;
using std::string;

class SuffArray {
public:
    SuffArray(const string& str) : str(str),
                                   suff_arr(str.size()),
                                   classes(str.size()) {
        construct_suff_arr();
    }
    size_t count_num_of_diff_substrings();
private:
    void construct_suff_arr();
    void calc_initial_step();
    void calc_step(size_t k);
    vector<size_t> calc_lcp(const string& str);
    inline size_t num_in_alphabet(char letter);
    string str;
    vector<size_t> suff_arr;
    vector<size_t> classes;
    size_t num_of_classes;
};

int main() {
    string str;
    std::cin >> str;
    SuffArray suff_arr(str + '`');
    std::cout << suff_arr.count_num_of_diff_substrings();
    return 0;
}

size_t SuffArray::num_in_alphabet(char letter) {
    return (size_t)(letter - '`');
}

size_t SuffArray::count_num_of_diff_substrings() {
    suff_arr.erase(suff_arr.begin());
    str.pop_back();
    auto num_of_subs = str.size() * str.size();
    vector<size_t> lcp = calc_lcp(str);
    for (size_t i = 0; i < str.size() - 1; ++i) {
        num_of_subs -= suff_arr[i];
        num_of_subs -= lcp[i];
    }
    num_of_subs-= suff_arr[str.size() - 1];
    return num_of_subs;
}

void SuffArray::construct_suff_arr() {
    calc_initial_step();
    for (size_t k = 0; (1 << k) < str.size(); ++k) {
        calc_step(k);
    }
}

void SuffArray::calc_initial_step() {
    vector<size_t> counter(ALPHABET_SIZE, 0);
    for (auto i: str) {
        ++counter[num_in_alphabet(i)];
    }
    for (size_t i = 0; i < counter.size() - 1; ++i) {
        counter[i + 1] += counter[i];
    }
    for (size_t i = 0; i < suff_arr.size(); ++i) {
        suff_arr[--counter[num_in_alphabet(str[i])]] = i;
    }
    classes[suff_arr[0]] = 0;
    num_of_classes = 1;
    for (int j = 1; j < suff_arr.size(); ++j) {
        if (str[suff_arr[j]] != str[suff_arr[j - 1]]) {
            classes[suff_arr[j]] = num_of_classes;
            ++num_of_classes;
        }
        classes[suff_arr[j]] = num_of_classes - 1;
    }
}

void SuffArray::calc_step(size_t k) {
    vector<size_t> new_suff_arr(suff_arr);
    for (int i = 0; i < str.size(); ++i) {
        new_suff_arr[i] = (new_suff_arr[i] - (1 << k) + str.size()) % str.size();
    }
    vector<size_t> counter(num_of_classes, 0);
    for (auto i: new_suff_arr) {
        ++counter[classes[i]];
    }
    for (size_t i = 0; i < counter.size() - 1; ++i) {
        counter[i + 1] += counter[i];
    }
    for (int i = str.size() - 1; i >= 0; --i) {
        suff_arr[--counter[classes[new_suff_arr[i]]]] = new_suff_arr[i];
    }
    vector<size_t> new_classes(str.size());
    new_classes[suff_arr[0]] = 0;
    num_of_classes = 1;
    for (int i = 1; i < str.size(); ++i) {
        if (classes[suff_arr[i - 1]] != classes[suff_arr[i]] ||
            classes[(suff_arr[i - 1] + (1 << k)) % str.size()] !=
            classes[(suff_arr[i] + (1 << k)) % str.size()]) {
            num_of_classes++;
        }
        new_classes[suff_arr[i]] = num_of_classes - 1;
    }
    classes = std::move(new_classes);
}

vector<size_t> SuffArray::calc_lcp(const string &str) {
    vector<size_t> lcp(str.size());
    vector<size_t> pos(str.size());
    for (size_t i = 0; i < str.size(); ++i) {
        pos[suff_arr[i]] = i;
    }
    for (size_t i = 0, k = 0; i < str.size(); ++i) {
        if (k > 0) {
            --k;
        }
        if (pos[i] == str.size() - 1) {
            k = 0;
            continue;
        }
        size_t j = suff_arr[pos[i] + 1];
        while ((max(i + k, j + k) < str.size()) && (str[i + k] == str[j + k])) {
            ++k;
        }
        lcp[pos[i]] = k;
    }
    return lcp;
}