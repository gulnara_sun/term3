#include <iostream>
#include <map>
#include <string>
#include <vector>

using std::map;
using std::string;
using std::vector;

struct node {
    node(): l(-1),
            r(-1),
            par(-1),
            link(-1) {};
    node(int l, int r, int par) : l(l),
                                  r(r),
                                  par(par),
                                  link(-1) {};
    int l;  // edge to this node denotes substring str[l;r)
    int r;
    int par;
    int link;
    map<char, int> next;
};

struct position {
    position(): V(0), L(0) {};
    position(int V, int L) : V(V), L(L) {};
    position(const position& other): V(other.V), L(other.L) {};
    int V; // vertex we a going up from
    int L; // how many we are going up until the position (in letters)
};

class SuffTree {
public:
    explicit SuffTree(const string& str1, const string& str2) : size_of_tree(0) ,
                                                                tree_nodes(2 * (str1.size() + str2.size()) + 1),
                                                                pos(0, 0),
                                                                str(str1 + str2), 
                                                                str1(str1),
                                                                str2(str2) {
        make_tree();
    };
    void print_two_suff_tree() {
        make_two_suff_tree();
        std::cout << size_of_tree << '\n';
        for (size_t vertex = 1; vertex < two_suff_tree.size(); ++vertex) {
            int parent = two_suff_tree[vertex].par;
            int l = two_suff_tree[vertex].l;
            int r = two_suff_tree[vertex].r;
            if (l < str1.size() && r >= str1.size()) {
                r = str1.size();
            }
            if (l < str1.size()) {
                std::cout << parent << " " << "0 " << l << " " << r << "\n";
            } else {
                l -= str1.size();
                r -= str1.size();
                std::cout << parent << " " << "1 " << l << " " << r << "\n";
            }
        }
    }
    
private:
    int calc_leng(int v) {
        return tree_nodes[v].r - tree_nodes[v].l;
    }
    int add_to_parent(int l, int r, int par);
    int split_edge(position pos);
    position read_char(position pos, char c);
    position add_char(position pos, size_t i);
    position go_down_fast(int v, int l , int r);
    int link(int v);
    void make_tree();
    void renumerate(int v, std::vector<size_t> &prev_order, size_t &new_order);
    void make_two_suff_tree();

    size_t size_of_tree;
    position pos;
    string str;
    vector<node> tree_nodes;
    string str1;
    string str2;
    vector<node> two_suff_tree;
};

int main() {
    string s1, s2;
    std::cin >> s1 >>s2;
    SuffTree st(s1, s2);
    st.print_two_suff_tree();
    return 0;
}

void SuffTree::renumerate(int v, std::vector<size_t>& prev_order, size_t& new_order) {
    prev_order[v] = new_order++;
    for (auto item : tree_nodes[v].next) {
        renumerate(item.second, prev_order, new_order);
    }
}

void SuffTree::make_two_suff_tree() {
    std::vector<size_t> prev_order(size_of_tree, 0);
    size_t current_vertex_index = 0;
    renumerate(0, prev_order, current_vertex_index);

    two_suff_tree.assign(size_of_tree, node(0, 0, 0));
    for (size_t v = 1; v < size_of_tree; ++v) {
        two_suff_tree[prev_order[v]].par = static_cast<int>(prev_order[tree_nodes[v].par]);
        two_suff_tree[prev_order[v]].l = tree_nodes[v].l;
        two_suff_tree[prev_order[v]].r = tree_nodes[v].r;
    }
}

position SuffTree::go_down_fast(int v, int l, int r) {
    if (l == r) {
        return position(v, 0);
    }
    while(true) {
        v = tree_nodes[v].next[str[l]];
        if (calc_leng(v) >= r - l) {
            return position(v, calc_leng(v) - r +l);
        }
        l += calc_leng(v);
    }
}

int SuffTree::link(int v) {
    if (tree_nodes[v].link == -1) {
        tree_nodes[v].link = split_edge(go_down_fast(link(tree_nodes[v].par), tree_nodes[v].l + (tree_nodes[v].par == 0), tree_nodes[v].r));
    }
    return tree_nodes[v].link;
}

int SuffTree::add_to_parent(int l, int r, int par) {
    size_t new_ind = size_of_tree++;
    tree_nodes[new_ind] = node(l, r, par);
    return tree_nodes[par].next[str[l]] = new_ind;
}

//                           split_edge
//         @  <- par                          @  <- par
//   down { \                    =>            \
//        {  \                                  \
//            .  <- pos          =>              @  <- mid
//        up { \                                /
//           {  \                =>            /
//               @  <- v                      @  <- v

int SuffTree::split_edge(position pos) {
    int v = pos.V;
    int up = pos.L;
    int down = calc_leng(v) - up;
    if (up == 0) {
        return v;
    }
    if (down == 0) {
        return tree_nodes[v].par;
    }
    int mid = add_to_parent(tree_nodes[v].l, tree_nodes[v].l + down, tree_nodes[v].par);
    tree_nodes[v].l += down;
    tree_nodes[v].par = mid;
    tree_nodes[mid].next[str[tree_nodes[v].l]] = v;
    return mid;
}

// tries to go forward on the tree be given character
position SuffTree::read_char(position pos, char c) {
    int v = pos.V;
    int up = pos.L;
    // if we are on the edge, then check whether the next letter is a given character or not
    if (up != 0) {
        return str[tree_nodes[v].r - up] == c ? position(v, up - 1) : position(-1, -1);
    }
    // if wa are in vertex, then go down to the next vertex by edge,
    // which starts with given char and then go up to the r position
    else {
        auto it = tree_nodes[v].next.find(c);
        int w = it != tree_nodes[v].next.end() ? tree_nodes[v].next[c] : -1;
        return w != -1 ? position(w, calc_leng(w) - 1) : position(-1, -1);
    }
}

position SuffTree::add_char(position pos, size_t i) {
    while(true) {
        position npos = read_char(pos, str[i]);
        if (npos.V != -1) {
            return npos;
        }
        else {
            int mid = split_edge(pos);
            add_to_parent(i, str.size(), mid);
            pos = position(link(mid), 0);
            if (mid == 0) {
                return pos;
            }
        }
    }
}

void SuffTree::make_tree() {
    size_of_tree = 1;
    node root;
    root.link = 0;
    tree_nodes[0] = root;
    for (size_t i = 0; i < str.size(); ++i) {
        pos = add_char(pos, i);
    }
}