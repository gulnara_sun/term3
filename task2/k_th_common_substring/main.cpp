#include <algorithm>
#include <cmath>
#include <vector>
#include <iostream>
#include <string>

const size_t ALPHABET_SIZE = 28;

using std::vector;
using std::max;
using std::string;

vector<size_t> calc_lcp(const string& str, const vector<size_t>& suff_arr) {
    vector<size_t> lcp(str.size());
    vector<size_t> pos(str.size());
    for (size_t i = 0; i < str.size(); ++i) {
        pos[suff_arr[i]] = i;
    }
    for (size_t i = 0, k = 0; i < str.size(); ++i) {
        if (k > 0) {
            --k;
        }
        if (pos[i] == str.size() - 1) {
            k = 0;
            continue;
        }
        size_t j = suff_arr[pos[i] + 1];
        while ((max(i + k, j + k) < str.size()) && (str[i + k] == str[j + k])) {
            ++k;
        }
        lcp[pos[i]] = k;
    }
    return lcp;
}

class SuffArray {
public:
    SuffArray(const string& str, size_t size_of_str_0) : str(str),
                                                        suff_arr(str.size()),
                                                        classes(str.size()),
                                                        size_of_str_0(size_of_str_0),
                                                        num_of_string(str.size()),
                                                        num_of_classes(0) { 
        construct_suff_arr();
    }
    string find_k_th_common_substring(int64_t k);
private:
    void construct_suff_arr();
    void calculate_num_of_string();
    void calc_initial_step();
    void calc_step(size_t k);
    string str;
    vector<size_t> suff_arr;
    vector<size_t> classes;
    vector<short> num_of_string;
    size_t num_of_classes;
    size_t size_of_str_0;
};



int main() {
    string str0, str1;
    std::cin >> str0 >> str1;
    SuffArray sm(str0 + '`' + str1 + '_', str0.size());
    u_int64_t k;
    std::cin >> k;
    std::cout << sm.find_k_th_common_substring(k);

    return 0;
}

void SuffArray::calculate_num_of_string() {
    for (const auto &i : suff_arr) {
        i < size_of_str_0 ? num_of_string[i] = 0 : num_of_string[i] = 1;
    }
}

string SuffArray::find_k_th_common_substring(int64_t k) {
    vector<size_t > lcp = calc_lcp(str, suff_arr);
    calculate_num_of_string();
    
    string answer;
    size_t i = 0;
    while (num_of_string[suff_arr[i]] == num_of_string[suff_arr[i + 1]]) {
        i++;
    }
    uint64_t counter = 0;       
    int64_t min_lcp = 0;
    while (i < suff_arr.size() - 1 && counter < k) {
        uint32_t current_lcp = lcp[i];
        answer = str.substr(suff_arr[i], current_lcp);
        if (current_lcp > min_lcp) {
            counter += current_lcp - min_lcp;
        }

        min_lcp = current_lcp;
        ++i;
        if (counter == k) {
            break;
        }

        while (i + 1 < suff_arr.size() && num_of_string[suff_arr[i]] == num_of_string[suff_arr[i + 1]]) {
            current_lcp = lcp[i];
            if (current_lcp < min_lcp) {
                min_lcp = current_lcp;
            }
            i++;
        }
    }
    if (i == suff_arr.size() - 1 && answer.size() < k) {
        return "-1";
    }
    if (counter > k) {
        while (counter != k) {
            answer.pop_back();
            counter--;
        }
    }
    return answer;
}

void SuffArray::construct_suff_arr() {
    calc_initial_step();
    for (size_t k = 0; (1 << k) < str.size(); ++k) {
        calc_step(k);
    }
}

void SuffArray::calc_initial_step() {
    vector<size_t> counter(ALPHABET_SIZE, 0);
    for (auto i: str) {
        ++counter[i - '_'];
    }
    for (size_t i = 0; i < counter.size() - 1; ++i) {
        counter[i + 1] += counter[i];
    }
    for (size_t i = 0; i < suff_arr.size(); ++i) {
        suff_arr[--counter[str[i] - '_']] = i;
    }
    classes[suff_arr[0]] = 0;
    num_of_classes = 1;
    for (int j = 1; j < suff_arr.size(); ++j) {
        if (str[suff_arr[j]] != str[suff_arr[j - 1]]) {
            classes[suff_arr[j]] = num_of_classes;
            ++num_of_classes;
        }
        classes[suff_arr[j]] = num_of_classes - 1;
    }
}

void SuffArray::calc_step(size_t k) {
    vector<size_t> new_suff_arr(suff_arr);
    for (int i = 0; i < str.size(); ++i) {
        new_suff_arr[i] = (new_suff_arr[i] - (1 << k) + str.size()) % str.size();
    }
    vector<size_t> counter(num_of_classes, 0);
    for (auto i: new_suff_arr) {
        ++counter[classes[i]];
    }
    for (size_t i = 0; i < counter.size() - 1; ++i) {
        counter[i + 1] += counter[i];
    }
    for (int i = str.size() - 1; i >= 0; --i) {
        suff_arr[--counter[classes[new_suff_arr[i]]]] = new_suff_arr[i];
    }
    vector<size_t> new_classes(str.size());
    new_classes[suff_arr[0]] = 0;
    num_of_classes = 1;
    for (int i = 1; i < str.size(); ++i) {
        if (classes[suff_arr[i - 1]] != classes[suff_arr[i]] ||
            classes[(suff_arr[i - 1] + (1 << k)) % str.size()] !=
            classes[(suff_arr[i] + (1 << k)) % str.size()]) {
            num_of_classes++;
        }
        new_classes[suff_arr[i]] = num_of_classes - 1;
    }
    classes = std::move(new_classes);
}
